import React from 'react'
import './Outlet.css'

import arrow from '../next.png'


class OutletContainer extends React.Component {
    constructor (props) {
        super (props)
    }
    render () {
        console.log (this.props)
        if (this.props.data.outletData == null || this.props.data.outletData == "") {
            return (
                <div></div>
            )
        }
        let numberOfSuboutlets = this.props.data.outletData.length
        let subOutlets = []
        let finalOutlet

       

        for (let i = 1; i<numberOfSuboutlets-1; i++) {
            subOutlets.push (<SubOutlet data={this.props.data.outletData[i]}/>)
        }
        
        if (numberOfSuboutlets == 1) {
            return (
                <div className="main-outlet-container">
                    <FirstOutlet onlyOne = "true" data={this.props.data.outletData[0]}/>
                    {subOutlets}
                    {finalOutlet}
                </div>
            )
        } else {
            finalOutlet = <FinalOutlet data={this.props.data.outletData[numberOfSuboutlets-1]}/>
        }
        return (
            <div className="main-outlet-container">
                <FirstOutlet data={this.props.data.outletData[0]}/>
                {subOutlets}
                {finalOutlet}
            </div>
        )
    }
}

function SubOutlet (props) {
    let styles = {}
    if (props.data.waterFlow == true) {
        styles = {backgroundColor : "#00e838"}
    } else {
        styles = {backgroundColor : "#e80000"}
    }
    return (
        <div className="sub-outlet-container">
            <div className="vertical-container">
                <div className="vertical-pipe"></div>
                <div className="vertical-connector"></div>
            </div>
            <div className="horizontal-pipe" back>
                { props.data.waterFlow
                ? <img src={arrow} />
                : <div> </div>
                }
            </div>
            <div style={styles} className="sub-tank">
                {props.data.readingValue}
            </div>
            <div className="pipe-end">
                <div className="outlet-outflow-content">
                    {props.data.text}
                </div>
            </div>
        </div>
    )
}

function FirstOutlet (props) {
    let connector
    if (props.onlyOne == "true") {
        connector = <div className="horizontal-connector-single"></div>
    } else {
        connector = <div className="horizontal-connector"></div>
    }
    let styles = {}
    if (props.data.waterFlow == true) {
        styles = {backgroundColor : "#00e838"}
    } else {
        styles = {backgroundColor : "#e80000"}
    }
    return (
        <div className="sub-outlet-container">
            <div className="horizontal-container">
                <div className="connecting-horizontal-pipe"></div>
                {connector}
                <div className="connecting-horizontal-pipe-secondary">
                    { props.data.waterFlow
                    ? <img src={arrow} />
                    : <div> </div>
                    }
                </div>
            </div>
            <div style = {styles} className="sub-tank">
                {props.data.readingValue}
            </div>
            <div className="pipe-end">
                <div className="outlet-outflow-content">
                    {props.data.text}
                </div>
            </div>
        </div>
    )
}

function FinalOutlet (props) {
    let styles = {}
    if (props.data.waterFlow == true) {
        styles = {backgroundColor : "#00e838"}
    } else {
        styles = {backgroundColor : "#e80000"}
    }
    return (
        <div className="sub-outlet-container">
            <div className="vertical-container">
                <div className="final-vertical-pipe"></div>
                <div className="final-vertical-connector"></div>
            </div>
            <div className="horizontal-pipe">
                { props.data.waterFlow
                ? <img src={arrow} />
                : <div> </div>
                }
            </div>
            <div style = {styles} className="sub-tank">
                {props.data.readingValue}
            </div>
            <div className="pipe-end">
                <div className="outlet-outflow-content">
                    {props.data.text}
                </div>
            </div>
        </div>
    )
}

export default OutletContainer