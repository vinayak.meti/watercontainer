import React from 'react'

import './Inlet.css'
import arrow from '../next.png'

class InletContainer extends React.Component {
    constructor (props) {
        super (props)
    }

    render () {
        if (this.props.data.inletData == null || this.props.data.inletData == "") {
            return (
                <div></div>
            )
        }
        let numberOfSubInlets = this.props.data.inletData.length
        let subInlets = []

        for (let i = 0; i < numberOfSubInlets; i++) {
            subInlets.push (<SubInlet data={this.props.data.inletData[i]}/>)
        }

        return (
            <div className="main-inlet-container">
                {subInlets}
            </div>
        )
    }
}

function SubInlet (props) {
    let styles = {}
    if (props.data.waterFlow == true) {
        styles = {backgroundColor : "#00e838"}
    } else {
        styles = {backgroundColor : "#e80000"}
    }

    return (
        <div className="sub-inlet-container">
            <div className="horizontal-pipe"> 
                <div className="inlet-inflow-content">
                    {props.data.text}
                </div>
            </div>
            <div style={styles} className="sub-tank">
                {props.data.readingValue}
            </div>
            <div className="pipe-end">
                {props.data.waterFlow 
                ? <img src={arrow} />
                : <div></div>
                }
            </div>

        </div>   
    )
}

export default InletContainer