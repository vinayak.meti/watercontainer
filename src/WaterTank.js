import React from 'react';
import './WaterTank.css';

import OutletContainer from './components/Outlet.js'
import InletContainer from './components/Inlet.js'


class MainBody extends React.Component {
    constructor (props) {
        super (props)

    }

    render () {
        const outletData = 
        {
            outletData:[
                {
                    id:1, waterFlow:true , text: "someText", readingValue:23
                },
                {
                    id:2, waterFlow:true , text: "someText2", readingValue:44
                },
                {
                    id:3, waterFlow:false , text: "someText3", readingValue:55
                },
                {
                    id:4, waterFlow:true , text: "someText5", readingValue:66
                },
                {
                    id:5, waterFlow:true , text: "someText6", readingValue:77
                },
                {
                    id:6, waterFlow:true , text: "someText6", readingValue:77
                },
                {
                    id:7, waterFlow:true , text: "someText6", readingValue:77
                },
                {
                    id:8, waterFlow:true , text: "someText6", readingValue:77
                },
                {
                    id:9, waterFlow:true , text: "someText6", readingValue:77
                },
                {
                    id:10, waterFlow:true , text: "someText6", readingValue:77
                }

            ]
        }

        const inletData = 
        {
            inletData:[
                {
                    id:1, waterFlow:true , text: "someText", readingValue:1
                },
                {
                    id:2, waterFlow:true , text: "someText2", readingValue:2
                },
                {
                    id:3, waterFlow:false , text: "someText3", readingValue:3
                },
                {
                    id:4, waterFlow:true , text: "someText5", readingValue:4
                },
                {
                    id:5, waterFlow:true , text: "someText6", readingValue:5
                },
                {
                    id:6, waterFlow:true , text: "someText6", readingValue:5
                },
                {
                    id:7, waterFlow:true , text: "someText6", readingValue:5
                },
                {
                    id:8, waterFlow:true , text: "someText6", readingValue:5
                },
                {
                    id:9, waterFlow:true , text: "someText6", readingValue:5
                },
                {
                    id:10, waterFlow:true , text: "someText6", readingValue:5
                },                                                 
            ]
        }

        return (
            <div className="main-container">
                <InletContainer data={inletData} />
                <div className="tank">
                <WaterContainer level="70"/>
                </div>
                <OutletContainer data={outletData} />
            </div>
        );
    }
}

/* function InletContainer (props) {
    let inlets = [];
    for (let i=0; i<props.numbers; i++) {
        inlets.push (<RenderInlet />);
    }

    return (
        <div className="inlet-container">
            {inlets}
        </div>
    )
}
 */
/* function RenderInlet () {
    return (
        <div className="inlet">
            <img src= {noflow} />
        </div>
    )
}
 */
/* function OutletContainer (props) {
    let outlets = [];
    for (let i=0; i<props.numbers; i++) {
        outlets.push (<RenderOutlet />)
    }

    return (
        <div className="outlet-container">
            {outlets}
        </div>
    )
} */

/* function RenderOutlet () {
    return (
        <div className="outlet">
            <img src= {outflow} />
        </div>
    ) 

} */

function WaterContainer (props) {
    let styles = {
        height : props.level +'%'
    }
    return (
        <div style={styles} className="water"></div>
    )
}


export default MainBody