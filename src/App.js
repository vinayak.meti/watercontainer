import React from 'react';
import './App.css';

const computers = ["mainframe", "supercomputer", "minicomputer","microcomputer"];
const rams = ["ddr2", "ddr3", "ddr4"];

class App extends React.Component {
  constructor (props) {
    super (props);
  }
  
  render () {
    return (
      <div>
        <Header title = "Types of computers"></Header>
        <ItemList items = {computers} > </ItemList> 
        
        <Header title = "Types of rams"></Header>
        <ItemList items = {rams} ></ItemList>
      </div>
    );
  }
}

function Header (props) {
  return (
    <div>
      <h2> {props.title}</h2>
    </div>
  );
}

function ItemList (props) {
  const items = props.items;
  const listItems = items.map (( item ) =>
  <li key={item}> {item}</li>
  );

  return (
  <ul> {listItems}</ul>
  );
}


export default App;
